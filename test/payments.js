'use strict';

const supertest = require('supertest');
const app = require('../app');

const api = supertest(app);
const test = require('tape');
const queue = require('kue').createQueue();  

//Dummy orider object
const dummyOrder = {
    title: 'Order #4kSvjL_Qx',
    paymentToken: '4kSvjL_Qx',
    orderID: '1a2b3c4',
    received: true,
    receivedAt: new Date('December 24, 2015 23:59:59'),
    createdAt: new Date('December 24, 2015 23:58:59'),
    productID: '5d6e6f',
    customer: {
        firstName: 'A',
        lastName: 'Person',
        email: 'example@example.com',
        address: '1234 somewhere lane, ? USA 12345'
    }
}

test('Creating payments and processing items with the queue', t => {  
  // put kue into test mode
  queue.testMode.enter();

  queue.createJob('payment', dummyOrder).save();
  queue.createJob('payment', dummyOrder).save();

  t.equal(queue.testMode.jobs.length, 2, 'There should be two jobs');
  t.equal(queue.testMode.jobs[0].type, 'payment', 'The jobs should be of type payment');
  t.equal(queue.testMode.jobs[0].data, dummyOrder, 'The job data should be intact');

  // Clear and exit test mode
  queue.testMode.clear();
  queue.testMode.exit()
  t.end();
});

test('Receiving and processing payments', t => {
    api
        .post('/payments')
        .send(dummyOrder)
        .end((err, res) => {
            const order = res.body;

            //Check for response
            t.ok(res.body, 'should response with a body');
            //Check to see if the order is intact
            t.equals(order.received, true, 'Should have been received');
            t.equals(order.orderID, dummyOrder.orderID, 'Order ID should be the same');
            t.equals(order.paymentToken, dummyOrder.paymentToken, 'Payment token should be the same');
            t.equals(order.productID, dummyOrder.productID, 'Product ID should be the same');
            t.end();

            // I ran into some unexpected behavior w/ supertest and tape where tape tests would appear to hang, even after calling t.end()
            process.exit();
        });
});
