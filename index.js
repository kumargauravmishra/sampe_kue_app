'use strict';
var Queue = require('bee-queue');

var redis = {
    "host": "localhost",
    "port": 6379
};
var queue = new Queue('job_queue', {
    redis: redis,
    isWorker: true
});

function newJob() {
    var job = queue.createJob('new_job');
    job.timeout(3000).retries(2).save(function(err, job) {
        console.log("Job created and saved");
    });
};

queue.process(function (job, done) {
  console.log('Processing job ' + job.id);
  return done();
});


setInterval(newJob, 1000);
