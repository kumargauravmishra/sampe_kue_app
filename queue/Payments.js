'use strict';

let config = {
    redis: {
        "host": "localhost",
        "port": 6379
    }
};

const queue = require('kue').createQueue(config);

queue.watchStuckJobs(6000);

queue.on('ready', () => {
    console.log("Queue is ready!");
});

queue.on('error', () => {
    //Handle connection error here
    console.error("There was an error in the main queue!");
    console.error(err);
    console.error(err.stack);
});


function createPayment(data, done) {
	queue.create('payment', data)
		.priority('critical')
		.attempts(8)
		.backoff(true)
		.removeOnComplete(false)
		.save((err) => {
			if(err){
				console.error(err);
				done(err);
			}
			if(!err){
				console.log("Done without errors!");
				done();
			}
		});
}

// Process up to 20 jobs concurrently
queue.process('payment', 20, function(job, done){  
  // other processing work here
  // ...
  // ...

  // Call done when finished
  done();
});

module.exports = {
	create: (data, done) => {
		createPayment(data, done);
	}
};
